import React from "react";


class Todo extends React.Component {
    onClickElement = (e) => {
        console.log(e.target.style.color);
        (e.target.style.color === 'red') ? e.target.style.color = 'green' : e.target.style.color = 'red';
    }

    render() {
        const { rows} = this.props
        return (
            <>
                {rows.map(item => <div style={item.color ? {color: 'green'} : {color: 'red'}} key={item.id}
                                       onClick={this.onClickElement}  >{item.title}</div>)}
            </>
        )
    }
}

/*function ColorfulText({children}) {
    return <span style={{color: 'green'}}>{children}</span>;
}
{song.isLiked ? '👍' : '👎'}

*/
export default Todo