import React from "react";
import './App.css';
import Todo from "./components/Todo";
class App extends React.Component {
    state = {
        rows: [],
        inputValue: ''
    }

    onAddElement = () => {
        if(this.state.inputValue !== ""){
            this.setState({rows: [...this.state.rows, {id: Math.random(), title: this.state.inputValue, color: 0}]})
            this.state.inputValue = "";
        }
    }
    onInputValueChange = (e) => {
        this.setState({inputValue: e.target.value})
    }


    render() {
        return (
            <>
                <h1>List to do :</h1>
                <div>
                    <Todo rows={this.state.rows} />
                </div>
                <button onClick={this.onAddElement}>Add element</button>
                <input type='text' value={this.state.inputValue} onChange={this.onInputValueChange} placeholder="input your  traffic "/>
            </>
        );
    }
}

export default App;
